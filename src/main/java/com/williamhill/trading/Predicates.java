package com.williamhill.trading;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Slf4j
public class Predicates {

    static Predicate<String> filterCsvRange(int expectedColumnsMin, int expectedColumnsMax) {
        return l -> {
            final String[] strings = LineSplitterUtil.splitEscaped(l, CsvConstants.CSV_SEPARATOR_CHAR, CsvConstants.CSV_SEPARATOR_ESCAPE_CHAR);
            if (strings.length >= expectedColumnsMin && strings.length <= expectedColumnsMax) {
                return true;
            } else {
                log.debug("ignore line:{}", l);
                return false;
            }
        };
    }

    static Predicate<String> filterCsv(int expectedColumns) {
        return l -> {
            final String[] strings = LineSplitterUtil.splitEscaped(l, CsvConstants.CSV_SEPARATOR_CHAR, CsvConstants.CSV_SEPARATOR_ESCAPE_CHAR);
            if (strings.length == expectedColumns) {
                return true;
            } else {
                log.debug("ignore line:{}", l);
                return false;
            }
        };
    }

    static Consumer<String> writeLine(BufferedWriter out) {
        return line -> {
            try {
                out.write(line);
                out.write("\n");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
    }
}
