package com.williamhill.trading;

public class ServiceException extends RuntimeException {

    private ServiceErrorMessage message;

    public ServiceException() {
    }

    public ServiceException(ServiceErrorMessage message) {
        super(message.getGeneralInfo());
        this.message = message;
    }

    public ServiceErrorMessage getResponseObject(){
        return message;
    }

}
