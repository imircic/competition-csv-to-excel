package com.williamhill.trading;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class CompetitionService implements CsvConstants {
    private Map<String, String> categoryMap;

    public CompetitionService() {
        categoryMap = new HashMap<>();
        categoryMap.put("94bc9ea1-aac1-45f0-a466-b73eb99b2fc3", "American Football");
        categoryMap.put("328a87d5-8f81-4b38-8943-067217d5890b", "Horse Racing");
        categoryMap.put("2ffd4c60-d90e-11e7-9296-cec278b6b50a", "Basketball");
        categoryMap.put("d10c7c4a-58eb-11e8-b701-bb6a475893e0", "Baseball");
        categoryMap.put("0ba3baee-f041-4a52-a65f-8ab8021ea21f", "Football");
        categoryMap.put("D56B96B3-6EB6-41A6-8ED2-7ACC5A1812BD", "Ice Hockey");
    }

    public void processCsv() throws IOException {
        final Reader in = new InputStreamReader(this.getClass().getResourceAsStream("/competitions.csv"), DEFAULT_CHARSET);

        Workbook workBook = new SXSSFWorkbook();
        SXSSFSheet sheet = (SXSSFSheet) workBook.createSheet("Sheet");
        FileOutputStream fileOutputStream = null;


        AtomicInteger rowNum = new AtomicInteger(0);
        Row headerRaw = sheet.createRow(rowNum.addAndGet(1));
        headerRaw.createCell(1).setCellValue("WhId");
        headerRaw.createCell(2).setCellValue("Name");
        headerRaw.createCell(3).setCellValue("Category");
        headerRaw.createCell(4).setCellValue("Location");

        try {
            CsvHandlerUtil.processLines(in,
                    Predicates.filterCsvRange(4, 6),
                    line -> {
                        Row currentRow = sheet.createRow(rowNum.addAndGet(1));

                        final String[] split = LineSplitterUtil.splitCsvEscaped(line);
                        log.debug("Uploading competition: {}", line);

                        currentRow.createCell(1).setCellValue(split[0]);
                        currentRow.createCell(2).setCellValue(split[1]);
                        if (!categoryMap.containsKey(split[2])) {
                            throw new IllegalArgumentException(String.format("Category with id: %s do not exist", split[2]));
                        }

                        currentRow.createCell(3).setCellValue(categoryMap.get(split[2]));
                        currentRow.createCell(4).setCellValue(split[3]);
                        //metadata are optional
                        if (split.length >= 5 && isEmpty(split[4])) {
                            String[] metadataEntry = split[4].split(CSV_LIST_SEPARATOR);
                            for (int i = 0; i < metadataEntry.length; i++) {
                                //first element in metadataEntryArray is name of metadata and second is value
                                String[] metadataEntryArray = metadataEntry[i].split(CSV_MAP_SEPARATOR);
                                if (metadataEntryArray.length < 2) {
                                    //ignore metadata if do not contain value
                                    continue;
                                }
                                int cellNumber = 5;
                                if (metadataEntryArray[1].contains(CSV_LIST_IN_MAP_SEPARATOR)) {
                                    List<String> listValues = Arrays.asList(metadataEntryArray[1].split(CSV_LIST_IN_MAP_SEPARATOR));
                                    while (headerRaw.getCell(cellNumber + i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) != null && !headerRaw.getCell(cellNumber + i).getStringCellValue().equals(metadataEntryArray[0])) {
                                        cellNumber++;
                                    }
                                    if (headerRaw.getCell(cellNumber + i) == null) {
                                        headerRaw.createCell(cellNumber + i).setCellValue(metadataEntryArray[0]);
                                    }
                                    currentRow.createCell(cellNumber + i).setCellValue(listValues.toString());
                                } else {
                                    while (headerRaw.getCell(cellNumber + i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) != null && !headerRaw.getCell(cellNumber + i).getStringCellValue().equals(metadataEntryArray[0])) {
                                        cellNumber++;
                                    }
                                    if (headerRaw.getCell(cellNumber + i) == null) {
                                        headerRaw.createCell(cellNumber + i).setCellValue(metadataEntryArray[0]);
                                    }
                                    currentRow.createCell(cellNumber + i).setCellValue(metadataEntryArray[1]);
                                }
                            }
                        }
                        if (split.length >= 6) {
                            String[] modelVariableEntry = split[5].split(CSV_LIST_SEPARATOR);
                            for (int i = 0; i < modelVariableEntry.length; i++) {
                                //first element in modelVariableEntryArray is name of modelVariable and second is value
                                String[] modelVariableEntryArray = modelVariableEntry[i].split(CSV_MAP_SEPARATOR);
                                if (modelVariableEntryArray[1].contains(CSV_LIST_IN_MAP_SEPARATOR)) {
                                    throw new IllegalArgumentException("incorrect data for model variable value " + modelVariableEntryArray[1]);
                                } else {
                                    int cellNumber = 5;
                                    while (headerRaw.getCell(cellNumber + i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) != null && !headerRaw.getCell(cellNumber + i).getStringCellValue().equals(modelVariableEntryArray[0])) {
                                        cellNumber++;
                                    }
                                    if (headerRaw.getCell(cellNumber + i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) == null) {
                                        headerRaw.createCell(cellNumber + i).setCellValue(modelVariableEntryArray[0]);
                                    }
                                    currentRow.createCell(cellNumber+i).setCellValue(modelVariableEntryArray[1]);
                                }
                            }
                        }
                        log.info("Writing competition {}", currentRow);
                    });
            fileOutputStream = new FileOutputStream("competitions-formatted.xlsx");
            workBook.write(fileOutputStream);
        } finally {
            try {
                workBook.close();
            } catch (IOException ioExObj) {
                log.error("Exception While Closing I/O Objects In convertCsvToXls() Method?=  " + ioExObj);
            }
        }

    }

    private boolean isEmpty(String s) {
        return s != null && !"".equals(s);
    }
}
