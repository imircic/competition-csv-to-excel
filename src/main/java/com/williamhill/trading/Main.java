package com.williamhill.trading;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main {
    public static void main(String[] args) {
        try {
            CompetitionService competitionService = new CompetitionService();
            competitionService.processCsv();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
