package com.williamhill.trading;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
public class CsvHandlerUtil {

    /**
     * @throws IOException is there is problem reading from the reader.
     * @throws CsvUploadException if the internal processing by consumer cannot be done for specific line.
     */
    public static void processLines(Reader in, Predicate<String> filter, Consumer<String> consumer) throws IOException {
        List<CsvLineErrorMessage> processingErrors = new ArrayList<>();
        AtomicInteger lineNumIncrementor = new AtomicInteger();
        try (
                BufferedReader reader = new BufferedReader(in);
        ) {
            reader.lines()
                    .filter(line -> {
                        lineNumIncrementor.incrementAndGet();
                        return true;
                    })
                    .filter(line -> line != null && !"".equals(line.trim()))
                    .filter(line -> {
                        if (filter.test(line)) {
                            return true;
                        }
                        processingErrors.add(new CsvLineErrorMessage(
                                lineNumIncrementor.intValue(),
                                "Ignored line",
                                line ));
                        return false;
                    })
                    .forEach(l -> {
                        int lineNum = lineNumIncrementor.intValue();
                        try {
                            consumer.accept(l);
                        } catch (Exception e) {
                            processingErrors.add(new CsvLineErrorMessage(
                                    lineNum,
                                    e.getMessage(),
                                    l ));
                        }

                    });
        }
        log.info("Done processing {} lines.", lineNumIncrementor.intValue());
        if (!processingErrors.isEmpty()) {
            throw new CsvUploadException(processingErrors);
        }
    }

    public static String makeCsvList(List<String> metadataList) {
        //its necessary that in case of list we have at list one & in value (even when we have only one value),
        // so during reimport system recognize metadata like list
        if (metadataList.size() == 1) {
            return (metadataList.get(0) instanceof String ? escapeMetadataSpecialChars(metadataList.get(0)) : metadataList.get(0)) + "&";
        } else {
            return metadataList.stream().map(e -> e instanceof String ? escapeMetadataSpecialChars(e) : e).collect(Collectors.joining("&"));
        }
    }

    public static String escapeMetadataSpecialChars(String name) {
        return name.replaceAll(",|\\&|=|;", "");
    }


    public static String escapeComma(String name) {
        return name.replaceAll(",", "");
    }
}
