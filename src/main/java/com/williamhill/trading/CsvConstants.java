package com.williamhill.trading;

public interface CsvConstants {

    String DEFAULT_CHARSET = "UTF-8";
    char CSV_SEPARATOR_CHAR = ',';
    char CSV_SEPARATOR_ESCAPE_CHAR = '\\';
    String CSV_LIST_SEPARATOR = ";";
    String CSV_MAP_SEPARATOR = "=";
    String CSV_LIST_IN_MAP_SEPARATOR = "&";

    String COMPETITOR_MAP_DOWNLOAD_FILE_NAME = "competitor_mappings.csv";
    String COMPETITION_MAP_DOWNLOAD_FILE_NAME = "competition_mappings.csv";
    String CATEGORY_MAP_DOWNLOAD_FILE_NAME = "category_mappings.csv";
}
