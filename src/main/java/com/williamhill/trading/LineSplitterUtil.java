package com.williamhill.trading;

import java.util.ArrayList;
import java.util.List;

public class LineSplitterUtil {


    public static String [] splitCsvEscaped(String line) {
        return splitEscaped(line, CsvConstants.CSV_SEPARATOR_CHAR, CsvConstants.CSV_SEPARATOR_ESCAPE_CHAR);
    }

    public static String [] splitEscaped(String line, char separatorChar, char escapeChar) {
        StringBuilder sb = new StringBuilder();
        List<String> result = new ArrayList<>();
        for (int i=0; i < line.length(); i++) {
            char ch = line.charAt(i);
            boolean toSplit = true;
            if (ch == escapeChar) {
                char next = (i+1 < line.length()) ? line.charAt(i+1) : escapeChar;
                if (next == escapeChar) {
                    i++;
                    ch = escapeChar;
                } else if (next == separatorChar) {
                    i++;
                    ch = separatorChar;
                    toSplit = false;
                }
            }

            if (ch == separatorChar && toSplit) {
                result.add(sb.toString());
                sb.delete(0, sb.length());
            } else {
                sb.append(ch);
            }
        }
        result.add(sb.toString());
        return result.toArray(new String[result.size()]);
    }

}
