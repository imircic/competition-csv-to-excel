package com.williamhill.trading;

import lombok.Data;

import java.util.List;

@Data
public class CsvUploadException extends ServiceException {
    //  class definition


    public CsvUploadException(List<CsvLineErrorMessage> messages) {
        super(new ServiceErrorMessage("Failed to upload complete CSV", messages));
    }


}
