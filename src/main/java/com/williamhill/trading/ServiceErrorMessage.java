package com.williamhill.trading;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceErrorMessage {
    private String generalInfo;

    private Object details;
}
