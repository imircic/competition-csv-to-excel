package com.williamhill.trading;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CsvLineErrorMessage {

    private int lineNum;
    private String message;
    private String details;
    private final String type = "error";

    public CsvLineErrorMessage(int lineNum, String message) {
        this.lineNum = lineNum;
        this.message = message;
    }
}
